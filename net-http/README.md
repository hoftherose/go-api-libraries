# GOLANG API start

To run the project simply execute `go run` or `docker compose up --build`.

This is simply a template to get more familiar with the net/http library. Due to the fact that this library does not support wildcards, this small project implements regex wildcards in a RegexHandler. 
