package utils

import (
	"net/http"
	"regexp"
)

type Any interface{}

type Handler struct {
	pattern  regexp.Regexp
	function func(http.ResponseWriter, *http.Request, map[string]string)
	// Handler.ServeHTTP
}

type RegexHandler struct {
	routes []Handler
}

func (h *RegexHandler) HandleFunc(pattern regexp.Regexp, handle func(http.ResponseWriter, *http.Request, map[string]string)) {
	h.routes = append(h.routes, Handler{pattern, handle})
}

func (h RegexHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	for _, route := range h.routes {
		if route.pattern.MatchString(r.URL.Path) {
			vars := ReSubMatchMap(&route.pattern, r.URL.Path)

			// vars := map[string]string{"name": "Hector"}
			route.function(w, r, vars)
			return
		}
	}
	http.NotFound(w, r)
}
