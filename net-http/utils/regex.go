package utils

import (
	"regexp"
)

func ReSubMatchMap(r *regexp.Regexp, str string) map[string]string {
	match := r.FindStringSubmatch(str)
	subMatchMap := make(map[string]string)
	for i, name := range r.SubexpNames() {
		if i != 0 {
			subMatchMap[name] = match[i]
		}
	}

	return subMatchMap
}

func StrToRegex(pattern string) regexp.Regexp {
	regex, _ := regexp.Compile(pattern)
	return *regex
}
