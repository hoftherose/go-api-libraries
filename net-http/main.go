package main

import (
	"fmt"
	"log"
	"net/http"

	"github.com/hoftherose/go-api-libraries/net-http/utils"
)

func main() {
	var regexHandler utils.RegexHandler

	regexHandler.HandleFunc(utils.StrToRegex("/(?P<name>[a-zA-Z]+)"), helloWorld)
	regexHandler.HandleFunc(utils.StrToRegex("/"), helloWorld)

	err := http.ListenAndServe(":8080", regexHandler)
	if err != nil {
		log.Fatalf("Fatal error, msg: %v", err)
	}
}

func helloWorld(w http.ResponseWriter, r *http.Request, vars map[string]string) {
	fmt.Fprintf(w, "<h1>Hello World %s</h1>", vars["name"])
}
