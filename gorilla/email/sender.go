package email

import (
	"errors"
	"fmt"
	"net/http"

	"github.com/gorilla/mux"
)

const Prefix = "/email"

type user struct {
	username string
	password string
}

type Email struct {
	dir string
}

type Sender struct {
	email       Email
	credentials user
}

func (s *Sender) sendEmail(e Email) error {
	fmt.Printf(e.dir)
	fmt.Printf(s.email.dir)
	fmt.Printf(s.credentials.username)
	fmt.Printf(s.credentials.password)
	return errors.New("error, testing error")
}

func (s *Sender) makeHandle() func(http.ResponseWriter, *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		vars := mux.Vars(r)
		destEmail := Email{vars["dest"]}
		err := s.sendEmail(destEmail)

		if err != nil {
			fmt.Fprintf(w, "Error sending email: %s", err)
		}
	}
}

func LoadEmailRoutes(r *mux.Router) {
	s := Sender{Email{"email@gmail.com"}, user{"user", "password"}}

	r.HandleFunc(Prefix, func(w http.ResponseWriter, r *http.Request) { fmt.Fprint(w, "Hello") })
	r.HandleFunc(Prefix+"/{dest}", s.makeHandle())
}
