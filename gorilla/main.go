package main

import (
	"fmt"
	"net/http"

	"github.com/gorilla/mux"
	"github.com/hoftherose/go-api-libraries/gorilla/calculator"
	"github.com/hoftherose/go-api-libraries/gorilla/email"
)

func main() {
	r := mux.NewRouter()
	calculator.LoadArithmeticRoutes(r)
	email.LoadEmailRoutes(r)
	r.HandleFunc("/{name:[a-zA-Z]+}", helloWorld)
	r.HandleFunc("/", helloWorld)

	http.ListenAndServe(":8080", r)
}

func helloWorld(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	fmt.Fprintf(w, "Hello World %s", vars["name"])
}
