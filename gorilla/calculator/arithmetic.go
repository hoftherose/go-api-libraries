package calculator

import (
	"fmt"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
)

const Prefix = "/calculator"

func showValue(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	x, err1 := strconv.Atoi(vars["x"])
	if err1 != nil {
		fmt.Fprintf(w, "Error, could not convert %s to int", vars["x"])
	}
	fmt.Fprintf(w, "%d", x)
}

func addValues(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	x, err1 := strconv.Atoi(vars["x"])
	y, err2 := strconv.Atoi(vars["y"])
	if err1 != nil || err2 != nil {
		fmt.Fprintf(w, "Error, could not complete sum between %s and %s", vars["x"], vars["y"])
	}
	fmt.Fprintf(w, "%d", x+y)
}

func subValues(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	x, err1 := strconv.Atoi(vars["x"])
	y, err2 := strconv.Atoi(vars["y"])
	if err1 != nil || err2 != nil {
		fmt.Fprintf(w, "Error, could not complete sum between %s and %s", vars["x"], vars["y"])
	}
	fmt.Fprintf(w, "%d", x-y)
}

func multValues(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	x, err1 := strconv.Atoi(vars["x"])
	y, err2 := strconv.Atoi(vars["y"])
	if err1 != nil || err2 != nil {
		fmt.Fprintf(w, "Error, could not complete sum between %s and %s", vars["x"], vars["y"])
	}
	fmt.Fprintf(w, "%d", x*y)
}

func divValues(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	x, err1 := strconv.Atoi(vars["x"])
	y, err2 := strconv.Atoi(vars["y"])
	if err1 != nil || err2 != nil {
		fmt.Fprintf(w, "Error, could not complete sum between %s and %s", vars["x"], vars["y"])
	}
	fmt.Fprintf(w, "%f", float64(x)/float64(y))
}

func LoadArithmeticRoutes(r *mux.Router) {
	r.HandleFunc(Prefix, func(w http.ResponseWriter, r *http.Request) { fmt.Fprint(w, "Hello") })
	r.HandleFunc(Prefix+"/{x:\\d+}", showValue)
	r.HandleFunc(Prefix+"/sum/{x:\\d+}/{y:\\d+}", addValues)
	r.HandleFunc(Prefix+"/subtract/{x:\\d+}/{y:\\d+}", subValues)
	r.HandleFunc(Prefix+"/multiply/{x:\\d+}/{y:\\d+}", multValues)
	r.HandleFunc(Prefix+"/divide/{x:\\d+}/{y:\\d+}", divValues)
}
